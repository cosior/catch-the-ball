using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace CatchTheBall2
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Point resolution = new Point(640, 480);
        MainController mainController = new MainController();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            #region Window Size Resolution Method.
            graphics.PreferredBackBufferWidth = resolution.X;
            graphics.PreferredBackBufferHeight = resolution.Y;
            #endregion
        }

        protected override void Initialize()
        {
            mainController.Initialize(GraphicsDevice);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            mainController.LoadContent(Content);
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            mainController.Update(gameTime);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || mainController.IsGameExit)
                this.Exit();

            if (mainController.IsMouseVisible) { this.IsMouseVisible = true; }
            else { this.IsMouseVisible = false; }

            if (mainController.IsResolutionChange)
            {
                if (resolution != mainController.GetCurrentResolution)
                {
                    resolution = mainController.GetCurrentResolution;
                    graphics.PreferredBackBufferWidth = resolution.X;
                    graphics.PreferredBackBufferHeight = resolution.Y;
                    graphics.ApplyChanges();
                    mainController.LoadOnlyMenu();
                    mainController.IsResolutionChange = false;
                }
                else mainController.IsResolutionChange = false;
            }

            if (mainController.IsFullScreen)
            {
                graphics.ToggleFullScreen();
                mainController.IsFullScreen = false;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.FrontToBack, SaveStateMode.None);
            mainController.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
