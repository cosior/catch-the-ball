﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CatchTheBall2
{
    class Player
    {
        #region Variables
        GraphicsDevice graphics;
        ContentManager content;
        Texture2D texture, textureSingle, textureTwo;
        Vector2 position;
        Color color;
        int windowWidth, windowHeight, score;
        float speed = 20.0f, velocityY, velocityX, scale;
        public Rectangle rectangle;
        MySpriteFont font = new MySpriteFont();
        bool singlePlayer, initializeSingle = true, initializeTwo = true;
        #endregion

        #region Public Main Methods

        public virtual void LoadContent(ContentManager content)
        {
            this.content = content;

            font.LoadContent(content);
            textureSingle = content.Load<Texture2D>(@"Images/Player/PlayerBar");
            textureTwo = content.Load<Texture2D>(@"Images/Player/barTop");
        }

        public void Initialize(GraphicsDevice graphics)
        {
            this.graphics = graphics;

            color = Color.White;
            windowHeight = graphics.Viewport.Height;
            windowWidth = graphics.Viewport.Width;
            velocityX = 0;
            velocityY = 0;
            score = 0;

            if (singlePlayer)
            {
                position.X = (graphics.Viewport.Width - 150 * scale) / 2.0f;
                position.Y = graphics.Viewport.Height - (graphics.Viewport.Height / 6);
                initializeSingle = false;
            }
            else
            {
                position.X = (windowWidth - 150 * scale) / 2.0f;
                position.Y = (windowHeight / 6);
                initializeTwo = false;
            }

        }

        public void Update(GameTime gameTime)
        {
            #region Controls For Player & Physics Mod

            KeyboardState keyboard = Keyboard.GetState();

            if (singlePlayer)
            {
                if (initializeSingle) { Initialize(graphics); } // to initialize single and two players at correct time.
                texture = textureSingle;
                if (keyboard.IsKeyDown(Keys.Up)) { velocityY -= speed; }
                if (keyboard.IsKeyDown(Keys.Down)) { velocityY += speed; }
                if (keyboard.IsKeyDown(Keys.Right)) { velocityX += speed; }
                if (keyboard.IsKeyDown(Keys.Left)) { velocityX -= speed; }
            }
            else
            {
                if (initializeTwo) { Initialize(graphics); } // to initialize single and two players at correct time.
                texture = textureTwo;
                if (keyboard.IsKeyDown(Keys.W)) velocityY -= speed;
                if (keyboard.IsKeyDown(Keys.S)) velocityY += speed;
                if (keyboard.IsKeyDown(Keys.D)) velocityX += speed;
                if (keyboard.IsKeyDown(Keys.A)) velocityX -= speed;
            }

            position.Y += velocityY * (float)gameTime.ElapsedGameTime.TotalSeconds; // physics mod
            position.X += velocityX * (float)gameTime.ElapsedGameTime.TotalSeconds;
            velocityY *= 0.98f;
            velocityX *= 0.98f;

            #endregion

            #region Window Limits
            windowHeight = graphics.Viewport.Height; // Fixed "Resolution Change" Player Window Limits.
            windowWidth = graphics.Viewport.Width;

            if (singlePlayer)
            {
                if (position.Y < (windowHeight / 2))
                {
                    position.Y = (windowHeight / 2);
                    velocityY = 0;
                }
                if (position.Y > (windowHeight - texture.Height * scale))
                {
                    position.Y = windowHeight - texture.Height * scale;
                    velocityY = 0;
                }
                if (position.X < 0)
                {
                    position.X = 0.0f;
                    velocityX = 0;
                }
                if (position.X > windowWidth - texture.Width * scale)
                {
                    position.X = windowWidth - texture.Width * scale;
                    velocityX = 0;
                }
            }
            else
            {
                if (position.Y < 0)
                {
                    position.Y = 0;
                    velocityY = 0;
                }
                if (position.Y > (windowHeight / 2) - texture.Height * scale)
                {
                    position.Y = ((windowHeight / 2) - texture.Height * scale);
                    velocityY = 0;
                }
                if (position.X < 0)
                {
                    position.X = 0.0f;
                    velocityX = 0;
                }
                if (position.X > windowWidth - texture.Width * scale)
                {
                    position.X = windowWidth - texture.Width * scale;
                    velocityX = 0;
                }
            }

            #endregion

            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (singlePlayer)
            {
                spriteBatch.Draw(textureSingle, rectangle, null, color, 0.0f, Vector2.Zero, SpriteEffects.None, 0.4f);
                font.singlePlayerScore(spriteBatch, graphics, score);
            }
            else
            {
                spriteBatch.Draw(textureTwo, rectangle, null, color, 0.0f, Vector2.Zero, SpriteEffects.None, 0.4f);
                font.TwoPlayerScore(spriteBatch, graphics, score);
            }
        }

        #endregion

        #region Propetries

        public bool IsPlayerSingle
        {
            get { return singlePlayer; }
            set { singlePlayer = value; }
        }

        public int ScoreValue
        {
            get { return score; }
            set { score = value; }
        }

        public Color SetColor
        {
            get { return color; }
            set { color = value; }
        }

        public float SetScale
        {
            set { scale = value; }
            get { return scale; }
        }

        #endregion
    }
}


