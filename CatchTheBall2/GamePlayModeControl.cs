﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace CatchTheBall2
{
    class GamePlayModeControl
    {
        #region Variables

        GraphicsDevice graphics;
        ContentManager content;
        Menu.menuState state = new Menu.menuState();
        Menu.menuState prevState = new Menu.menuState();
        BallBase ball = new BallBase();
        Player player = new Player();
        Player playerTwo = new Player();
        BallTwoPlayers ballTwo = new BallTwoPlayers();
        ExtraBall extraball = new ExtraBall();
        ExtraBall.ExtraBallMode extraBallMode = new ExtraBall.ExtraBallMode();

        bool resetLimit = true, ballDestroy, drawExtraBall, gamePause;
        int hitArea, hitAreaTwo, oldScore, oldScoreTwo, sidehit = 0;
        float scale,speed;

        #endregion

        #region Public Methods

        public void LoadContent(ContentManager content)
        {
            this.content = content;
            ball.LoadContent(content);
            player.LoadContent(content);
            playerTwo.LoadContent(content);
            ballTwo.LoadContent(content);
            extraball.LoadContent(content);
        }

        public void Initialize(GraphicsDevice  graphics)
        {
            this.graphics = graphics;
            // Initialize all Variables to Default Values. 
            ballDestroy = false;
            drawExtraBall = false;

            hitArea = 0;
            hitAreaTwo = 0;
            oldScore = 0;
            oldScoreTwo = 0;
            sidehit = 0;

            ball.Initialize(graphics);
            ball.SetSpeedRate = speed;
            ballTwo.Initialize(graphics);
            ballTwo.SetSpeedRate = speed;
            ballTwo.repositionTwoPlayers(ref ballTwo.position);
            ball.repositionUpper(ref ball.position);
            player.Initialize(graphics);
            player.IsPlayerSingle = true;
            playerTwo.Initialize(graphics);
            playerTwo.IsPlayerSingle = false;
            extraball.Initialize(graphics);
            extraball.SetSpeedRate = speed;
        }

        public void Update(GameTime gameTime)
        {
            if (player.SetScale != scale) // True only if resolution change
            {
                ball.SetScale = scale;
                ballTwo.SetScale = scale;
                player.SetScale = scale;
                playerTwo.SetScale = scale;
                player.Initialize(graphics); //reinitialize to correct center player with scale
                playerTwo.Initialize(graphics);
                extraball.SetScale = scale;

                if (scale == 0.6f) { speed = 2.0f * 0.7f; } // arrange ball speed according resolution
                if (scale == 0.8f) { speed = 2.0f * 0.9f; }
                if (scale == 1.0f) { speed = 2.0f * 1.0f; }

                ball.SetSpeedRate = speed;
                ballTwo.SetSpeedRate = speed;
                extraball.SetSpeedRate = speed;
                ball.Initialize(graphics); // reinitialize to correct speed rate according resolution
                ballTwo.Initialize(graphics);
                extraball.Initialize(graphics);
            }
            if (!gamePause)
            {
                player.Update(gameTime);

                if (drawExtraBall) { extraball.Update(gameTime); }
                
                if (state == Menu.menuState.singlePlayer)
                {
                    ball.Update(gameTime);
                    singlePlayerGameLogic(gameTime);
                }
                if (state == Menu.menuState.twoPlayer)
                {
                    playerTwo.Update(gameTime);
                    ballTwo.Update(gameTime);
                    twoPlayerGameLogic(gameTime);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            player.Draw(spriteBatch);
            if (drawExtraBall) { extraball.Draw(spriteBatch); }
            if (state == Menu.menuState.singlePlayer)
            {
                ball.Draw(spriteBatch);
            }
            if (state == Menu.menuState.twoPlayer)
            {
                playerTwo.Draw(spriteBatch);
                ballTwo.Draw(spriteBatch);
            }
            if (state == Menu.menuState.gameOver) // To Draw PrevState when Game Over.
            {
                if (prevState == Menu.menuState.singlePlayer)
                {
                    ball.Draw(spriteBatch);
                }
                if (prevState == Menu.menuState.twoPlayer)
                {
                    playerTwo.Draw(spriteBatch);
                    ballTwo.Draw(spriteBatch);
                }
            }
        }

        #endregion

        #region Private Methods

        private void singlePlayerGameLogic(GameTime gametime)
        {
            #region Count Times When Hit Floor And Reset Counter
            if (ball.IsBallHitBottom())
            {
                hitArea++;
                oldScore = player.ScoreValue;
            }
            switch (hitArea)
            {
                case 0:
                    player.SetColor = Color.White;
                    break;
                case 1:
                    player.SetColor = Color.LightSlateGray;
                    break;
                case 2:
                    player.SetColor = Color.OrangeRed;
                    break;
            }
            if ((player.ScoreValue % (oldScore + 1000)) == 0 && player.ScoreValue != 0)
            {
                if (hitArea != 0 || player.SetColor != Color.White)
                {
                    ball.PlayReward();
                    hitArea = 0;
                }
            }
            if (hitArea == 3) { ballDestroy = true; }
            #endregion

            #region Add Score To Player When Hit Ball
            if (player.rectangle.Intersects(ball.rectangle))
            {
                player.ScoreValue++;
                ball.IntesectDirection();
            }
            #endregion

            #region 300 Score Reposition Ball
            if ((player.ScoreValue % 300) == 0 && player.ScoreValue != 0 && resetLimit)
            {
                ball.repositionUpper(ref ball.position);
                ball.PlayReposition();
                resetLimit = false;
            }
            if ((player.ScoreValue % 300) == 1) { resetLimit = true; }
            #endregion

            #region Update Speed of Ball After 5000 Points
            if (player.ScoreValue > 5000)
            {
                if ((player.ScoreValue % 2000) == 0 && resetLimit)
                {
                    ball.speedRate += 0.2f;
                    ball.speedX = ball.speedRate;
                    ball.speedY = ball.speedRate;
                    ball.PlaySpeedUp();
                }
            }
            #endregion

            #region Add Extra Ball Logic And Fuctions
            if (ball.IsBallHitLeft() || ball.IsBallHitRight()) { sidehit++; }
            if ((sidehit % 5) == 0 && player.ScoreValue != 0 && sidehit != 0 && !drawExtraBall)
            {
                drawExtraBall = true;
                extraball.drawSound.Play();
                sidehit = 0;
            }
            if (extraball.rectangle.Intersects(ball.rectangle) && drawExtraBall)
            {
                drawExtraBall = false;
                extraball.drawSound.Play();
                extraball.Initialize(graphics);
                sidehit = 0;
            }
            extraBallMode = extraball.ballMode;

            if (extraball.IsBallHitLeft()) extraball.speedY -= 1.25f;// extra movement
            if (extraball.IsBallHitBottom()) extraball.speedX += 1.0f;
            if (extraball.IsBallHitRight()) extraball.speedY += 1.25f;
            if (extraball.IsBallHitTop()) extraball.speedX -= 1.5f;

            if (extraball.rectangle.Intersects(player.rectangle) && drawExtraBall)
            {
                switch (extraBallMode)
                {
                    case ExtraBall.ExtraBallMode.Evil:
                        extraball.evilSound.Play();
                        hitArea++; // Player Louse one Life.
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Good:
                        if (hitArea != 0) // Only If Player Lost Life.
                        {
                            hitArea -= 1;
                            extraball.goodSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.ExtraScore:
                        extraball.extrascoreSound.Play();
                        player.ScoreValue += 500;
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.MinusScore:
                        if (player.ScoreValue >= 300)// Only if Player Score >= 300 Points
                        {
                            player.ScoreValue -= 300;
                            extraball.minusScoreSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Stick:
                        if (ball.speedRate >= 2.2f) // Only If Ball Speed > 2.2f
                        {
                            ball.speedRate -= 0.1f;
                            extraball.stickSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.Initialize(graphics);
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                    case ExtraBall.ExtraBallMode.None://reset mode after other cases done.
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                }
            }
            #endregion
        }

        private void twoPlayerGameLogic(GameTime gameTime)
        {
            #region Count Times When Hit Floor And Reset Counter
            if (ballTwo.IsBallHitBottom())
            {
                hitArea++;
                oldScore = player.ScoreValue;
            }
            if (ballTwo.IsBallHitTop())
            {
                hitAreaTwo++;
                oldScoreTwo = playerTwo.ScoreValue;
            }

            switch (hitArea)
            {
                case 0:
                    player.SetColor = Color.White;
                    break;
                case 1:
                    player.SetColor = Color.LightSlateGray;
                    break;
                case 2:
                    player.SetColor = Color.OrangeRed;
                    break;
            }
            switch (hitAreaTwo)
            {
                case 0:
                    playerTwo.SetColor = Color.White;
                    break;
                case 1:
                    playerTwo.SetColor = Color.LightSlateGray;
                    break;
                case 2:
                    playerTwo.SetColor = Color.OrangeRed;
                    break;
            }

            if ((player.ScoreValue % (oldScore + 1000)) == 0 && player.ScoreValue != 0)
            {
                if (hitArea != 0 || player.SetColor != Color.White)
                {
                    ball.PlayReward();
                    hitArea = 0;
                }
            }
            if ((playerTwo.ScoreValue % (oldScoreTwo + 1000)) == 0 && playerTwo.ScoreValue != 0)
            {
                if (hitAreaTwo != 0 || playerTwo.SetColor != Color.White)
                {
                    ballTwo.PlayReward();
                    hitAreaTwo = 0;
                }
            }

            if (hitArea == 3 || hitAreaTwo == 3) { ballDestroy = true; }
            #endregion

            #region Add Score To Player When Hit Ball
            if (player.rectangle.Intersects(ballTwo.rectangle))
            {
                player.ScoreValue++;
                ballTwo.IntesectDirection();
            }
            if (playerTwo.rectangle.Intersects(ballTwo.rectangle))
            {
                playerTwo.ScoreValue++;
                ballTwo.IntesectDirection();
            }
            #endregion

            #region 300 Score Reposition Ball
            if (((player.ScoreValue + playerTwo.ScoreValue) % 300) == 0 && (player.ScoreValue + playerTwo.ScoreValue) != 0 && resetLimit)
            {
                ballTwo.repositionTwoPlayers(ref ballTwo.position);
                ballTwo.PlayReposition();
                resetLimit = false;
            }
            if (((player.ScoreValue + playerTwo.ScoreValue) % 300) == 1) { resetLimit = true; }
            #endregion

            #region Update Speed of Ball After 5000 Points
            if ((player.ScoreValue + playerTwo.ScoreValue) > 5000)
            {
                if (((player.ScoreValue + playerTwo.ScoreValue) % 1000) == 0 && resetLimit)
                {
                    ballTwo.speedRate += 0.2f;
                    ballTwo.speedX = ball.speedRate;
                    ballTwo.speedY = ball.speedRate;
                    ballTwo.PlaySpeedUp();
                }
            }
            #endregion

            #region Add Extra Ball Logic And Fuctions
            if (ballTwo.IsBallHitLeft() || ballTwo.IsBallHitRight())
            {
                sidehit++;
            }
            if ((sidehit % 5) == 0 && (player.ScoreValue + playerTwo.ScoreValue) != 0 && sidehit != 0 && !drawExtraBall)
            {
                drawExtraBall = true;
                extraball.drawSound.Play();
                sidehit = 0;
            }
            if (extraball.rectangle.Intersects(ballTwo.rectangle) && drawExtraBall)
            {
                drawExtraBall = false;
                extraball.drawSound.Play();
                extraball.Initialize(graphics);
                sidehit = 0;
            }
            extraBallMode = extraball.ballMode;

            if (extraball.IsBallHitLeft()) extraball.speedY -= 1.25f;// extra movement
            if (extraball.IsBallHitBottom()) extraball.speedX += 1.0f;
            if (extraball.IsBallHitRight()) extraball.speedY += 1.55f;
            if (extraball.IsBallHitTop()) extraball.speedX -= 1.5f;

            if (extraball.rectangle.Intersects(player.rectangle) && drawExtraBall)
            {
                switch (extraBallMode)
                {
                    case ExtraBall.ExtraBallMode.Evil:
                        extraball.evilSound.Play();
                        hitArea++;
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Good:
                        if (hitArea != 0)
                        {
                            hitArea -= 1;
                            extraball.goodSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.ExtraScore:
                        extraball.extrascoreSound.Play();
                        player.ScoreValue += 500;
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.MinusScore:
                        if (player.ScoreValue >= 300)
                        {
                            player.ScoreValue -= 300;
                            extraball.minusScoreSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Stick:
                        if (ballTwo.speedRate >= 2.2f)
                        {
                            ballTwo.speedRate -= 0.1f;
                            extraball.stickSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.Initialize(graphics);
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                    case ExtraBall.ExtraBallMode.None://reset mode after first case done.
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                }
            }
            if (extraball.rectangle.Intersects(playerTwo.rectangle) && drawExtraBall)
            {
                switch (extraBallMode)
                {
                    case ExtraBall.ExtraBallMode.Evil:
                        extraball.evilSound.Play();
                        hitAreaTwo++;
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Good:
                        if (hitAreaTwo != 0)
                        {
                            hitAreaTwo -= 1;
                            extraball.goodSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.ExtraScore:
                        extraball.extrascoreSound.Play();
                        playerTwo.ScoreValue += 500;
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.MinusScore:
                        if (playerTwo.ScoreValue >= 300)
                        {
                            playerTwo.ScoreValue -= 300;
                            extraball.minusScoreSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        extraball.Initialize(graphics);
                        break;
                    case ExtraBall.ExtraBallMode.Stick:
                        if (ballTwo.speedRate >= 2.2f)
                        {
                            ballTwo.speedRate -= 0.1f;
                            extraball.stickSound.Play();
                        }
                        else { extraball.drawSound.Play(); }
                        drawExtraBall = false;
                        sidehit = 0;
                        extraball.Initialize(graphics);
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                    case ExtraBall.ExtraBallMode.None://reset mode after first case done.
                        extraball.ballMode = ExtraBall.ExtraBallMode.None;
                        break;
                }
            }
            #endregion
        }

        #endregion

        #region Properties

        public float SetScale
        {
            set { scale = value; }
        }

        public Menu.menuState SetState
        {
            set { state = value; }
            get { return state; }
        }

        public Menu.menuState SetPrevState
        {
            set { prevState = value; }
        }

        public bool SetIsGamePause
        {
            set { gamePause = value; }
        }

        public bool GetIsBallDestroy
        {
            get { return ballDestroy; }
        }

        public int GetScore
        {
            get { return player.ScoreValue + playerTwo.ScoreValue; }
        }

        #endregion
    }
}
