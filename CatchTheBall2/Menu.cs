﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace CatchTheBall2
{
    class Menu
    {
        #region Variables

        ContentManager content;
        GraphicsDevice graphics;
        MouseState prevMouseState, currentMouseState;
        KeyboardState keyboard, preKeyboard;
        Texture2D mainMenuTitle, singlePlayerButton, twoPlayerButton, hiScoreButton, optionsButton, exitButton, exitButtonTrans,
            enterGameB, backButton, fullsceenButton, hiResButton, midResButton, lowResButton, musicOnOffButton, optionsTitle,
            menuBackround, splashScreen, singlePlayerBackround, twoPlayerBackround, escapeMessage, pauseMessage, gameOverMessage;
        Rectangle mainMenuRectangle, pointerRectangle, singlePlayerBRectangle, twoPlayerBRectangle, hiScoreBRectangle,
            optionsBRectangle, exitBRegtangle, exitTransBRectangle, enterGameBRectangle, optionsTitleRectangle, backBRectangle,
            fullscreenBRectangle, hiResBRectangle, midResBRectangle, lowResBRectangle, musicOnOffBRectangle, menuBackroundRectangle,
            splashScreenRectangle, singlePlayerBackroundRectangle, twoPlayerBackroundRectangle, escapeMessageRectangle, pauseMessageRectangle,
            gameOverMessageRectangle;
        Vector2 pointerPosition;
        Color singlePlayerColor, twoPlayerColor, hiScoreColor, optionsColor, exitColor, exitTransColor, backColor, fullScreenColor,
            lowResColor, hiResColor, midResColor, musicOnOffColor, enterGameColor, pauseEffectColor;
        float scale = 0.6f;

        bool fullScreenToggle = false, applyChanges = false, showMouse = true, exit = false, muteMusic = false, gamepause = false,
            ballDestroy = false;

        public enum menuState
        {
            splashScreen, mainMenu, singlePlayer, twoPlayer, escapeState, hiScore, options, gameOver
        }
        public menuState currentState = new menuState();
        public menuState prevState = new menuState();
        public Point resolution;

        #endregion

        #region Public Methods

        public void LoadContent(ContentManager content)
        {
            this.content = content;

            #region Splash and Backround
            menuBackround = content.Load<Texture2D>(@"Images/SplashScreen/MenuBackround");
            menuBackroundRectangle = new Rectangle(0, 0, graphics.Viewport.Width, graphics.Viewport.Height);
            splashScreen = content.Load<Texture2D>(@"Images/SplashScreen/MainScreen");
            splashScreenRectangle = new Rectangle(0, 0, graphics.Viewport.Width, graphics.Viewport.Height);
            enterGameB = content.Load<Texture2D>(@"Images/SplashScreen/EnterGame");
            enterGameBRectangle = new Rectangle(((int)(graphics.Viewport.Width - enterGameB.Width * scale) / 2),
               (int)(graphics.Viewport.Height / 1.6f), (int)(enterGameB.Width * scale), (int)(enterGameB.Height * scale));
            exitButtonTrans = content.Load<Texture2D>(@"Images/SplashScreen/ExitTrans");
            exitTransBRectangle = new Rectangle(((int)(graphics.Viewport.Width - exitButtonTrans.Width * scale) / 2),
               (int)(graphics.Viewport.Height / 1.33f), (int)(exitButtonTrans.Width * scale), (int)(exitButtonTrans.Height * scale));
            #endregion

            #region Single And TwoPlayer Backrounds

            singlePlayerBackround = content.Load<Texture2D>(@"Images/StateBackrounds/backroundSingleP");
            singlePlayerBackroundRectangle = new Rectangle(0, 0, graphics.Viewport.Width, graphics.Viewport.Height);
            twoPlayerBackround = content.Load<Texture2D>(@"Images/StateBackrounds/BackroundTwoP");
            twoPlayerBackroundRectangle = new Rectangle(0, 0, graphics.Viewport.Width, graphics.Viewport.Height);

            #endregion

            #region Main Menu Items
            mainMenuTitle = content.Load<Texture2D>(@"Images/Menu/MenuTitle");
            mainMenuRectangle = new Rectangle(((int)(graphics.Viewport.Width - mainMenuTitle.Width * scale) / 2), 30,
                (int)(mainMenuTitle.Width * scale), (int)(mainMenuTitle.Height * scale));
            singlePlayerButton = content.Load<Texture2D>(@"Images/Menu/SinglePlayerB");
            singlePlayerBRectangle = new Rectangle(((int)(graphics.Viewport.Width - singlePlayerButton.Width * scale) / 2),
                (graphics.Viewport.Height / 4), (int)(singlePlayerButton.Width * scale), (int)(singlePlayerButton.Height * scale));
            twoPlayerButton = content.Load<Texture2D>(@"Images/Menu/TwoPlayerB");
            twoPlayerBRectangle = new Rectangle(((int)(graphics.Viewport.Width - twoPlayerButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 2.65f), (int)(twoPlayerButton.Width * scale), (int)(twoPlayerButton.Height * scale));
            hiScoreButton = content.Load<Texture2D>(@"Images/Menu/HiScoreB");
            hiScoreBRectangle = new Rectangle(((int)(graphics.Viewport.Width - hiScoreButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 2), (int)(hiScoreButton.Width * scale), (int)(hiScoreButton.Height * scale));
            optionsButton = content.Load<Texture2D>(@"Images/Menu/OptionsB");
            optionsBRectangle = new Rectangle(((int)(graphics.Viewport.Width - optionsButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 1.60f), (int)(optionsButton.Width * scale), (int)(optionsButton.Height * scale));
            exitButton = content.Load<Texture2D>(@"Images/Menu/ExitB");
            exitBRegtangle = new Rectangle(((int)(graphics.Viewport.Width - exitButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 1.33f), (int)(exitButton.Width * scale), (int)(exitButton.Height * scale));
            escapeMessage = content.Load<Texture2D>(@"Images/StateBackrounds/EscapeMessage");
            escapeMessageRectangle = new Rectangle(((int)(graphics.Viewport.Width - escapeMessage.Width * scale) / 2),
                (int)((graphics.Viewport.Height - escapeMessage.Height * scale) / 2), (int)(escapeMessage.Width * scale),
                (int)(escapeMessage.Height * scale));
            pauseMessage = content.Load<Texture2D>(@"Images/StateBackrounds/PauseMessage");
            pauseMessageRectangle = new Rectangle(((int)(graphics.Viewport.Width - pauseMessage.Width * scale) / 2),
                (int)((graphics.Viewport.Height - pauseMessage.Height * scale) / 2), (int)(pauseMessage.Width * scale),
                (int)(pauseMessage.Height * scale));
            gameOverMessage = content.Load<Texture2D>(@"Images/StateBackrounds/GameOverMessage");
            gameOverMessageRectangle = new Rectangle(((int)(graphics.Viewport.Width - gameOverMessage.Width * scale) / 2),
                (int)((graphics.Viewport.Height - gameOverMessage.Height * scale) / 2), (int)(gameOverMessage.Width * scale),
                (int)(gameOverMessage.Height * scale));
            #endregion

            #region Options Items
            optionsTitle = content.Load<Texture2D>(@"Images/Menu/Options/optionsTitle");
            optionsTitleRectangle = new Rectangle(((int)(graphics.Viewport.Width - optionsTitle.Width * scale) / 2), 30,
                (int)(optionsTitle.Width * scale), (int)(optionsTitle.Height * scale));
            musicOnOffButton = content.Load<Texture2D>(@"Images/Menu/Options/MusicOnOffB");
            musicOnOffBRectangle = new Rectangle(((int)(graphics.Viewport.Width - musicOnOffButton.Width * scale) / 2),
                (graphics.Viewport.Height / 4), (int)(musicOnOffButton.Width * scale), (int)(musicOnOffButton.Height * scale));
            fullsceenButton = content.Load<Texture2D>(@"Images/Menu/Options/FullScreenB");
            fullscreenBRectangle = new Rectangle(((int)(graphics.Viewport.Width - fullsceenButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 2.65f), (int)(fullsceenButton.Width * scale), (int)(fullsceenButton.Height * scale));
            lowResButton = content.Load<Texture2D>(@"Images/Menu/Options/LowResolutionB");
            lowResBRectangle = new Rectangle(((int)(graphics.Viewport.Width - lowResButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 2), (int)(lowResButton.Width * scale), (int)(lowResButton.Height * scale));
            midResButton = content.Load<Texture2D>(@"Images/Menu/Options/MidResolutionB");
            midResBRectangle = new Rectangle(((int)(graphics.Viewport.Width - midResButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 1.60f), (int)(midResButton.Width * scale), (int)(midResButton.Height * scale));
            hiResButton = content.Load<Texture2D>(@"Images/Menu/Options/HiResolutionB");
            hiResBRectangle = new Rectangle(((int)(graphics.Viewport.Width - hiResButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 1.33f), (int)(hiResButton.Width * scale), (int)(hiResButton.Height * scale));
            backButton = content.Load<Texture2D>(@"Images/Menu/Options/BackB");
            backBRectangle = new Rectangle(((int)(graphics.Viewport.Width - backButton.Width * scale) / 2),
                (int)(graphics.Viewport.Height / 1.15f), (int)(backButton.Width * scale), (int)(backButton.Height * scale));
            #endregion
        }

        public void Initialize(GraphicsDevice graphics)
        {
            this.graphics = graphics;
            currentState = menuState.splashScreen;
            pauseEffectColor = Color.White;
        }

        public void Update(GameTime gameTime)
        {
            pointerRectangle = new Rectangle((int)pointerPosition.X, (int)pointerPosition.Y, 1, 1);

            currentMouseState = Mouse.GetState();
            keyboard = Keyboard.GetState();

            if (currentMouseState.X != prevMouseState.X || currentMouseState.Y != prevMouseState.Y) //Effect xna Mouse only
            {
                pointerPosition = new Vector2(currentMouseState.X, currentMouseState.Y);
            }

            switch (currentState)
            {
                case menuState.splashScreen:
                    #region SpalshScreen Code
                    if (keyboard.IsKeyDown(Keys.Enter)) { currentState = menuState.mainMenu; }
                    if (keyboard.IsKeyDown(Keys.Escape)) { exit = true; }
                    if (pointerRectangle.Intersects(enterGameBRectangle))
                    {
                        enterGameColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            currentState = menuState.mainMenu;
                        }
                    }
                    else enterGameColor = Color.White;
                    if (pointerRectangle.Intersects(exitTransBRectangle))
                    {
                        exitTransColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            exit = true;
                        }
                    }
                    else exitTransColor = Color.White;
                    break;
                    #endregion
                case menuState.mainMenu:
                    #region Main Menu Code
                    showMouse = true;
                    if (pointerRectangle.Intersects(singlePlayerBRectangle))
                    {
                        singlePlayerColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed)
                        {
                            currentState = menuState.singlePlayer;
                        }
                    }
                    else singlePlayerColor = Color.White;

                    if (pointerRectangle.Intersects(twoPlayerBRectangle))
                    {
                        twoPlayerColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed)
                        {
                            currentState = menuState.twoPlayer;
                        }
                    }
                    else twoPlayerColor = Color.White;

                    if (pointerRectangle.Intersects(hiScoreBRectangle))
                    {
                        hiScoreColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed)
                        {
                            currentState = menuState.hiScore;
                        }
                    }
                    else hiScoreColor = Color.White;

                    if (pointerRectangle.Intersects(optionsBRectangle))
                    {
                        optionsColor = Color.LightGray;

                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            currentState = menuState.options;
                        }
                    }
                    else optionsColor = Color.White;

                    if (pointerRectangle.Intersects(exitBRegtangle))
                    {
                        exitColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed) { exit = true; }
                    }
                    else exitColor = Color.White;
                    #endregion
                    break;
                case menuState.singlePlayer:
                case menuState.twoPlayer:
                    #region Single & Two Player Code
                    showMouse = false;
                    if (keyboard.IsKeyDown(Keys.P) && preKeyboard.IsKeyUp(Keys.P))
                    {
                        if (!gamepause) { gamepause = true; }
                        else gamepause = false;
                    }
                    if (keyboard.IsKeyDown(Keys.Escape))
                    {
                        if (!ballDestroy) // Game Is Still Playing
                        {
                            prevState = currentState;
                            currentState = menuState.escapeState;
                        }
                        else // Game Is Over Ball Destroy
                        {
                            currentState = menuState.mainMenu;
                        }
                    }
                    break;
                    #endregion
                case menuState.escapeState:
                    #region Escape State Code
                    gamepause = false; // reset Game Pause Flag
                    if (keyboard.IsKeyDown(Keys.Escape) && preKeyboard.IsKeyUp(Keys.Escape))
                    {
                        currentState = menuState.mainMenu;
                    }
                    if (keyboard.IsKeyDown(Keys.Enter))
                    {
                        currentState = prevState;
                    }
                    #endregion
                    break;
                case menuState.hiScore:
                    showMouse = true;
                    if (keyboard.IsKeyDown(Keys.Escape)) { currentState = menuState.mainMenu; }
                    break;
                case menuState.options:
                    #region Options Menu Code
                    showMouse = true;
                    if (pointerRectangle.Intersects(musicOnOffBRectangle))
                    {
                        musicOnOffColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            if (!muteMusic) { muteMusic = true; }
                            else muteMusic = false;
                        }
                    }
                    else musicOnOffColor = Color.White;
                    if (pointerRectangle.Intersects(fullscreenBRectangle))
                    {
                        fullScreenColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            fullScreenToggle = true;
                        }
                    }
                    else fullScreenColor = Color.White;
                    if (pointerRectangle.Intersects(lowResBRectangle))
                    {
                        lowResColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            resolution = new Point(640, 480);
                            scale = 0.6f;
                            applyChanges = true;
                        }
                    }
                    else lowResColor = Color.White;
                    if (pointerRectangle.Intersects(midResBRectangle))
                    {
                        midResColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            resolution = new Point(800, 600);
                            scale = 0.8f;
                            applyChanges = true;
                        }
                    }
                    else midResColor = Color.White;
                    if (pointerRectangle.Intersects(hiResBRectangle))
                    {
                        hiResColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                        {
                            resolution = new Point(1024, 768);
                            scale = 1.0f;
                            applyChanges = true;
                        }
                    }
                    else hiResColor = Color.White;
                    if (pointerRectangle.Intersects(backBRectangle))
                    {
                        backColor = Color.LightGray;
                        if (currentMouseState.LeftButton == ButtonState.Pressed)
                        {
                            currentState = menuState.mainMenu;
                        }
                    }
                    else backColor = Color.White;
                    #endregion
                    if (keyboard.IsKeyDown(Keys.Escape)) { currentState = menuState.mainMenu; }
                    break;
                case menuState.gameOver:
                    if (keyboard.IsKeyDown(Keys.Escape)) { currentState = menuState.mainMenu; }
                    break;
            }
            prevMouseState = currentMouseState;
            preKeyboard = keyboard;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            switch (currentState)
            {
                case menuState.splashScreen:
                    spriteBatch.Draw(menuBackround, menuBackroundRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    spriteBatch.Draw(splashScreen, splashScreenRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(enterGameB, enterGameBRectangle, null, enterGameColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.4f);
                    spriteBatch.Draw(exitButtonTrans, exitTransBRectangle, null, exitTransColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.4f);
                    break;
                case menuState.mainMenu:
                    spriteBatch.Draw(menuBackround, menuBackroundRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    spriteBatch.Draw(mainMenuTitle, mainMenuRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(singlePlayerButton, singlePlayerBRectangle, null, singlePlayerColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(twoPlayerButton, twoPlayerBRectangle, null, twoPlayerColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(hiScoreButton, hiScoreBRectangle, null, hiScoreColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(optionsButton, optionsBRectangle, null, optionsColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(exitButton, exitBRegtangle, null, exitColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    break;
                case menuState.singlePlayer:
                    GamePauseBallDestroyCheck(spriteBatch);
                    spriteBatch.Draw(singlePlayerBackround, singlePlayerBackroundRectangle, null, pauseEffectColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    break;
                case menuState.twoPlayer:
                    GamePauseBallDestroyCheck(spriteBatch);
                    spriteBatch.Draw(twoPlayerBackround, twoPlayerBackroundRectangle, null, pauseEffectColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    break;
                case menuState.escapeState:
                    spriteBatch.Draw(menuBackround, menuBackroundRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    spriteBatch.Draw(escapeMessage, escapeMessageRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    break;
                case menuState.hiScore:
                    spriteBatch.Draw(menuBackround, menuBackroundRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    break;
                case menuState.options:
                    spriteBatch.Draw(menuBackround, menuBackroundRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.0f);
                    spriteBatch.Draw(optionsTitle, optionsTitleRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(musicOnOffButton, musicOnOffBRectangle, null, musicOnOffColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(fullsceenButton, fullscreenBRectangle, null, fullScreenColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(lowResButton, lowResBRectangle, null, lowResColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(midResButton, midResBRectangle, null, midResColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(hiResButton, hiResBRectangle, null, hiResColor, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.2f);
                    spriteBatch.Draw(backButton, backBRectangle, null, backColor, 0.0f, Vector2.Zero, SpriteEffects.None, 0.2f);
                    break;
                case menuState.gameOver:
                    spriteBatch.Draw(gameOverMessage, gameOverMessageRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.8f);
                    if (prevState == menuState.singlePlayer)
                    {
                        spriteBatch.Draw(singlePlayerBackround, singlePlayerBackroundRectangle, null, pauseEffectColor, 0.0f,
                            Vector2.Zero, SpriteEffects.None, 0.0f);
                    }
                    if (prevState == menuState.twoPlayer)
                    {
                        spriteBatch.Draw(twoPlayerBackround, twoPlayerBackroundRectangle, null, pauseEffectColor, 0.0f,
                            Vector2.Zero, SpriteEffects.None, 0.0f);
                    }
                    break;
            }
        }

        #endregion

        #region Private Methods

        private void GamePauseBallDestroyCheck(SpriteBatch spriteBatch)
        {
            if (gamepause || ballDestroy)
            {
                prevState = currentState;
                pauseEffectColor = Color.LightGray;
                if (gamepause)
                {
                    spriteBatch.Draw(pauseMessage, pauseMessageRectangle, null, Color.White, 0.0f,
                        Vector2.Zero, SpriteEffects.None, 0.8f);
                }
                if (ballDestroy)
                {
                    currentState = menuState.gameOver;
                }
            }
            else pauseEffectColor = Color.White;
        }

        #endregion

        #region Properties

        public float GetScale
        {
            get { return scale; }
        }

        public bool IsBallDestroy
        {
            set { ballDestroy = value; }
        }

        public bool IsMouseVisible
        {
            get { return showMouse; }
        }

        public bool IsFullScreen
        {
            get { return fullScreenToggle; }
            set { fullScreenToggle = value; }
        }

        public bool IsResolutionChange
        {
            get { return applyChanges; }
            set { applyChanges = value; }
        }

        public Point GetCurrentResolution
        {
            get { return resolution; }
        }

        public bool IsGameExit
        {
            get { return exit; }
        }

        public bool IsMusicMute
        {
            get { return muteMusic; }
        }

        public bool IsGamePause
        {
            get { return gamepause; }
        }

        #endregion
    }
}

