﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CatchTheBall2
{
    class MySpriteFont
    {
        ContentManager content;
        GraphicsDevice graphics;

        SpriteFont font;

        public void LoadContent(ContentManager content)
        {
            this.content = content;
            font = content.Load<SpriteFont>(@"SpriteFont1");
        }

        public void singlePlayerScore(SpriteBatch spriteBatch, GraphicsDevice graphics, int score)
        {
            this.graphics = graphics;
            spriteBatch.DrawString(font, " Player 1: \n " + score.ToString(),
                   Vector2.Zero, Color.Red, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.8f);
        }

        public void TwoPlayerScore(SpriteBatch spriteBatch, GraphicsDevice graphics, int score)
        {
            this.graphics = graphics;
            spriteBatch.DrawString(font, " Player 2: \n " + score.ToString(),
                new Vector2(graphics.Viewport.Width - 150, 0), Color.Red, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.8f);
        }
    }
}

