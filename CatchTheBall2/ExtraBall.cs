﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CatchTheBall2
{
    class ExtraBall : BallBase
    {
        #region Variables

        int choice;
        Random rand = new Random();
        Texture2D evil, good, extrascore, minusscore, stick;
        public SoundEffect evilSound, goodSound, extrascoreSound, minusScoreSound, stickSound, drawSound;
        float scale;

        public enum ExtraBallMode
        {
            Evil, Good, ExtraScore, MinusScore, Stick, None
        }
        public ExtraBallMode ballMode;

        #endregion

        #region Public Methods.

        public override void LoadContent(ContentManager content)
        {
            //Textures
            evil = content.Load<Texture2D>(@"Images/Ball/Evil");
            good = content.Load<Texture2D>(@"Images/Ball/good");
            extrascore = content.Load<Texture2D>(@"Images/Ball/extrascore");
            minusscore = content.Load<Texture2D>(@"Images/Ball/minusscore");
            stick = content.Load<Texture2D>(@"Images/Ball/stick");
            //Sounds
            evilSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/EvilSound");
            goodSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/GoodSound");
            extrascoreSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/ExtraScoreSound");
            minusScoreSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/MinusScoreSound");
            stickSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/StickSound");
            drawSound = content.Load<SoundEffect>(@"Sounds/ExtraBall/DrawExtraBallSound");

            base.LoadContent(content);
        }

        public override void Initialize(GraphicsDevice graphics)
        {
            choice = rand.Next(0, 5);

            getDirection(ref speedX, ref speedY);

            reposition(ref position);

            base.Initialize(graphics);
        }

        public override void Update(GameTime gameTime)
        {
            switch (ballMode)
            {
                case ExtraBallMode.Evil:
                    SetTexture = evil;
                    break;
                case ExtraBallMode.Good:
                    SetTexture = good;
                    break;
                case ExtraBallMode.ExtraScore:
                    SetTexture = extrascore;
                    break;
                case ExtraBallMode.MinusScore:
                    SetTexture = minusscore;
                    break;
                case ExtraBallMode.Stick:
                    SetTexture = stick;
                    break;
            }
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)(SetTexture.Width * scale), (int)(SetTexture.Height * scale));
            moveBallSinglePlayer(ref position, SetTexture);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            switch (choice)
            {
                case 0:
                    ballMode = ExtraBallMode.Evil;
                    spriteBatch.Draw(evil, rectangle, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
                    break;
                case 1:
                    ballMode = ExtraBallMode.Good;
                    spriteBatch.Draw(good, rectangle, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
                    break;
                case 2:
                    ballMode = ExtraBallMode.ExtraScore;
                    spriteBatch.Draw(extrascore, rectangle, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
                    break;
                case 3:
                    ballMode = ExtraBallMode.MinusScore;
                    spriteBatch.Draw(minusscore, rectangle, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
                    break;
                case 4:
                    ballMode = ExtraBallMode.Stick;
                    spriteBatch.Draw(stick, rectangle, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
                    break;
            }
            //base.Draw(spriteBatch);
        }

        #endregion

        #region Proporties

        public Enum IsBallMode
        {
            get { return ballMode; }
        }

        public override float SetScale
        {
            set { scale = value; }
        }

        #endregion
    }
}


