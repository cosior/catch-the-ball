﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CatchTheBall2
{
    class BallBase
    {
        #region Variables

        GraphicsDevice graphics;
        ContentManager content;
        Random random = new Random();
        public Texture2D texture;
        public Vector2 position;
        Color color;
        SoundEffect ballHitWall, ballHitFloor, ballHitPlayer, reward, speedUp, repositionSound;
        public float speedX, speedY, speedRate;
        int windowHeight, windowWidth;
        float scale;

        public Rectangle rectangle;

        #endregion

        #region Virtual Public Main Methods

        public virtual void LoadContent(ContentManager content)
        {
            this.content = content;
            texture = content.Load<Texture2D>(@"Images/Ball/Marbles");
            loadSounds();
        }

        public virtual void Initialize(GraphicsDevice graphics)
        {
            this.graphics = graphics;

            windowHeight = graphics.Viewport.Height;
            windowWidth = graphics.Viewport.Width;

            speedX = speedRate;
            speedY = speedX;

            getDirection(ref speedX, ref speedY);

            color = Color.White;
        }

        public virtual void Update(GameTime gameTime)
        {
            moveBallSinglePlayer(ref position, texture);

            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(texture, rectangle, null, color, 0.0f, Vector2.Zero, SpriteEffects.None, 0.6f);
        }

        #endregion

        #region Public Methods

        public bool IsBallHitTop()
        {
            if (rectangle.Y < 0)
            {
                return true;
            }
            else return false;
        }

        public bool IsBallHitBottom()
        {
            if (rectangle.Y > graphics.Viewport.Height - rectangle.Height)
            {
                return true;
            }
            else return false;
        }

        public bool IsBallHitLeft()
        {
            if (rectangle.X < 0)
            {
                return true;
            }
            else return false;
        }

        public bool IsBallHitRight()
        {
            if (rectangle.X > (graphics.Viewport.Width - rectangle.Width))
            {
                return true;
            }
            else return false;
        }

        public void PlayReward()
        {
            reward.Play();
        }

        public void PlaySpeedUp()
        {
            speedUp.Play();
        }

        public void PlayReposition()
        {
            repositionSound.Play();
        }

        public void getDirection(ref float speedX, ref float speedY)
        {
            int randomDirection = random.Next(0, 4);

            if (randomDirection == 1) { speedX *= -1; }
            if (randomDirection == 2) { speedY *= -1; }
        }// tested working all directions

        public void moveBallSinglePlayer(ref Vector2 position, Texture2D texture)
        {
            position.X += speedX;
            position.Y += speedY;

            if (IsBallHitTop())
            {
                position.Y = 0;
                speedY *= -1;
                ballHitWall.Play(1.0f, 1.0f, 0.0f);
            }

            if (IsBallHitBottom())
            {
                position.Y = (graphics.Viewport.Height - rectangle.Height);
                speedY *= -1;
                ballHitFloor.Play();
            }

            if (IsBallHitLeft())
            {
                position.X = 0;
                speedX *= -1;
                ballHitWall.Play(1.0f, 0.0f, -1.0f);
            }

            if (IsBallHitRight())
            {
                position.X = (graphics.Viewport.Width - rectangle.Width);
                speedX *= -1;
                ballHitWall.Play(1.0f, 0.0f, 1.0f);
            }
        } // tested

        public void MoveBallTwoPlayer(ref Vector2 position, Texture2D texture)
        {
            position.X += speedX;
            position.Y += speedY;

            if (IsBallHitTop())
            {
                position.Y = 0;
                speedY *= -1;
                ballHitFloor.Play();
            }

            if (IsBallHitBottom())
            {
                position.Y = (graphics.Viewport.Height - rectangle.Height);
                speedY *= -1;
                ballHitFloor.Play();
            }

            if (IsBallHitLeft())
            {
                position.X = 0;
                speedX *= -1;
                ballHitWall.Play(1.0f, 0.0f, -1.0f);
            }

            if (IsBallHitRight())
            {
                position.X = (graphics.Viewport.Width - rectangle.Width);
                speedX *= -1;
                ballHitWall.Play(1.0f, 0.0f, 1.0f);
            }
        }

        public void IntesectDirection()
        {
            ballHitPlayer.Play(0.2f, 0.0f, 0.0f);
            speedY *= -1;
        }

        public void repositionUpper(ref Vector2 position)
        {
            position.X = (float)random.Next(0, windowWidth);
            position.Y = (float)random.Next(0, (windowHeight / 3));
        }

        public void reposition(ref Vector2 position)
        {
            position.X = (float)random.Next(0, windowWidth);
            position.Y = (float)random.Next(0, windowHeight);
        }

        public void repositionTwoPlayers(ref Vector2 position)
        {
            position.X = (float)random.Next(0, windowWidth);
            position.Y = (windowHeight / 2);
        }

        #endregion

        #region Private Methods

        private void loadSounds()
        {
            ballHitFloor = content.Load<SoundEffect>(@"Sounds/Ball/Punch");
            ballHitWall = content.Load<SoundEffect>(@"Sounds/Ball/pop1");
            ballHitPlayer = content.Load<SoundEffect>(@"Sounds/Ball/wine");
            reward = content.Load<SoundEffect>(@"Sounds/Ball/Reward");
            speedUp = content.Load<SoundEffect>(@"Sounds/Ball/SpeedUp");
            repositionSound = content.Load<SoundEffect>(@"Sounds/Ball/RepositionSound");
        }

        #endregion

        #region Properties

        public float SetSpeedRate
        {
            get { return speedRate; }
            set { speedRate = value; }
        }

        public Texture2D SetTexture
        {
            set { texture = value; }
            get { return texture; }
        }

        public Vector2 GetPosition
        {
            get { return position; }
        }

        public virtual float SetScale
        {
            set { scale = value; }
        }

        #endregion
    }
}


