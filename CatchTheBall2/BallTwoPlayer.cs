﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CatchTheBall2
{
    class BallTwoPlayers : BallBase
    {
        GraphicsDevice graphics;
        Color color;
        int windowHeight, windowWidth;
        float scale;

        public override void Initialize(GraphicsDevice graphics)
        {
            this.graphics = graphics;

            windowHeight = graphics.Viewport.Height;
            windowWidth = graphics.Viewport.Width;

            speedX = speedRate;
            speedY = speedX;

            getDirection(ref speedX, ref speedY);

            color = Color.White;

            base.Initialize(graphics);
        }

        public override void Update(GameTime gameTime)
        {
            MoveBallTwoPlayer(ref position, texture);
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));

            //base.Update(gameTime);
        }

        public override float SetScale
        {
            set { scale = value; }
        }
    }
}


