﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace CatchTheBall2
{
    class SoundManager
    {
        #region Variables

        ContentManager content;
        Menu.menuState state = new Menu.menuState();
        SoundEffect menuSound, gameSound1, gameSound2, gameSound3, gameSound4, gameOverSound;
        SoundEffectInstance menuSoundInstance, gameSoundInstance1, gameSoundInstance2, gameSoundInstance3, gameSoundInstance4;
        bool muteMusic, gamePause, ballDestroy, playonce = false;
        int score;

        #endregion

        public void LoadContent(ContentManager content)
        {
            this.content = content;
            menuSound = content.Load<SoundEffect>(@"Sounds/Menu/MainScreenSound");
            menuSoundInstance = menuSound.CreateInstance();
            gameSound1 = content.Load<SoundEffect>(@"Sounds/StateSounds/singleGame");
            gameSoundInstance1 = gameSound1.CreateInstance();
            gameSound2 = content.Load<SoundEffect>(@"Sounds/StateSounds/SingleGame2");
            gameSoundInstance2 = gameSound2.CreateInstance();
            gameSound3 = content.Load<SoundEffect>(@"Sounds/StateSounds/twoPlayerGame");
            gameSoundInstance3 = gameSound3.CreateInstance();
            gameSound4 = content.Load<SoundEffect>(@"Sounds/StateSounds/twoPlayerGame2");
            gameSoundInstance4 = gameSound4.CreateInstance();
            gameOverSound = content.Load<SoundEffect>(@"Sounds/StateSounds/GameOverSound");
        }

        public void Update(GameTime gameTime)
        {
            if (!muteMusic) // Music Is On
            {
                switch (state)
                {
                    case Menu.menuState.splashScreen:
                        menuSoundInstance.Play();
                        break;
                    case Menu.menuState.mainMenu:
                        menuSoundInstance.Play();
                        gameSoundInstance1.Stop(); // initialize music from begining
                        gameSoundInstance2.Stop();
                        gameSoundInstance3.Stop();
                        gameSoundInstance4.Stop();
                        break;
                    case Menu.menuState.singlePlayer:
                    case Menu.menuState.twoPlayer:
                        #region Sound logic in gamePlay
                        if (!ballDestroy) // Game Is Playing and Music is On
                        {
                            playonce = true;
                            if (!gamePause) // Game Is Not Pause
                            {
                                menuSoundInstance.Stop();
                                if (score >= 0 && score <= 5000) { gameSoundInstance1.Play(); }
                                if (score >= 5001 && score <= 10000) 
                                { 
                                    gameSoundInstance1.Stop(); 
                                    gameSoundInstance2.Play(); 
                                }
                                if (score >= 10001 && score <= 20000) 
                                {
                                    gameSoundInstance2.Stop();
                                    gameSoundInstance3.Play();
                                }
                                if (score >= 20001 && score <= 100000) 
                                {
                                    gameSoundInstance3.Stop(); 
                                    gameSoundInstance4.Play(); 
                                }
                            }
                            else // Game Is Pause and Music is On
                            {
                                gameSoundInstance1.Pause();
                                gameSoundInstance2.Pause();
                                gameSoundInstance3.Pause();
                                gameSoundInstance4.Pause();
                            }
                        }
                        #endregion
                        break;
                    case Menu.menuState.escapeState:
                        gameSoundInstance1.Pause();
                        gameSoundInstance2.Pause();
                        gameSoundInstance3.Pause();
                        gameSoundInstance4.Pause();
                        break;
                    case Menu.menuState.hiScore:
                        menuSoundInstance.Play();
                        break;
                    case Menu.menuState.options:
                        menuSoundInstance.Play();
                        break;
                    case Menu.menuState.gameOver:
                        if (playonce) { gameOverSound.Play(); playonce = false; }
                        gameSoundInstance1.Stop();
                        gameSoundInstance2.Stop();
                        gameSoundInstance3.Stop();
                        gameSoundInstance4.Stop();
                        break;
                }
            }
            else // Music Is Off
            {
                menuSoundInstance.Stop();
            }
        }

        #region Properties
        public bool IsMusicMute
        {
            set { muteMusic = value; }
        }
        public int GetScore
        {
            set { score = value; }
        }
        public bool IsBallDestroy
        {
            set { ballDestroy = value; }
        }
        public Menu.menuState GetState
        {
            set { state = value; }
        }
        public bool GetGamePause
        {
            set { gamePause = value; }
        }
        #endregion
    }
}
