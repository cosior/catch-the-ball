﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace CatchTheBall2
{
    class MainController
    {
        #region Variables

        ContentManager content;
        GraphicsDevice graphics;
        Menu menu = new Menu();
        SoundManager soundManager = new SoundManager();
        GamePlayModeControl gameControl = new GamePlayModeControl();
        bool initializeOnes = true;

        #endregion

        #region Public Methods

        public void LoadContent(ContentManager content)
        {
            this.content = content;
            LoadOnlyMenu();
            soundManager.LoadContent(content);
            gameControl.LoadContent(content);
        }

        public void Initialize(GraphicsDevice graphics)
        {
            this.graphics = graphics;
            menu.Initialize(graphics);
            gameControl.Initialize(graphics);
        }

        public void Update(GameTime gameTime)
        {
            menu.Update(gameTime);

            soundManager.Update(gameTime);
            updateDataInSoundManager();

            if (menu.currentState == Menu.menuState.mainMenu && initializeOnes) // initialize when game finish or exit
            {
                gameControl.Initialize(graphics);
                initializeOnes = false;
            }
            if (menu.currentState == Menu.menuState.singlePlayer || menu.currentState == Menu.menuState.twoPlayer)
            {
                initializeOnes = true;
                menu.IsBallDestroy = gameControl.GetIsBallDestroy;
                soundManager.GetScore = gameControl.GetScore;
                gameControl.Update(gameTime);
                gameControl.SetScale = menu.GetScale;
                gameControl.SetState = menu.currentState;
                gameControl.SetPrevState = menu.prevState;
                gameControl.SetIsGamePause = menu.IsGamePause;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            menu.Draw(spriteBatch);

            if (menu.currentState == Menu.menuState.singlePlayer || menu.currentState == Menu.menuState.twoPlayer
                || menu.currentState == Menu.menuState.gameOver)
            {
                gameControl.Draw(spriteBatch);
            }
        }

        public void LoadOnlyMenu()
        {
            menu.LoadContent(content);
        }

        #endregion

        #region Private Methods

        private void updateDataInSoundManager()
        {
            soundManager.GetState = menu.currentState;
            soundManager.IsMusicMute = menu.IsMusicMute;
            soundManager.GetGamePause = menu.IsGamePause;
        }

        #endregion

        #region Properties Used in Game1 Class

        public bool IsMouseVisible
        {
            get { return menu.IsMouseVisible; }
        }

        public bool IsGameExit
        {
            get { return menu.IsGameExit; }
        }

        public bool IsResolutionChange
        {
            get { return menu.IsResolutionChange; }
            set { menu.IsResolutionChange = value; }
        }

        public Point GetCurrentResolution
        {
            get { return menu.resolution; }
        }

        public bool IsFullScreen
        {
            get { return menu.IsFullScreen; }
            set { menu.IsFullScreen = value; }
        }

        #endregion
    }
}
